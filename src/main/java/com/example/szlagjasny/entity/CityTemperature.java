package com.example.szlagjasny.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class CityTemperature {
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String name;

    @Column
    private Float temperature;

    @Column
    private long readingId;

    public long getReadingId() {
        return readingId;
    }

    public CityTemperature setReadingId(long readingId) {
        this.readingId = readingId;
        return this;
    }

    public String getName() {
        return name;
    }

    public CityTemperature setName(String name) {
        this.name = name;
        return this;
    }

    public Float getTemperature() {
        return temperature;
    }

    public CityTemperature setTemperature(Float temperature) {
        this.temperature = temperature;
        return this;
    }
}
