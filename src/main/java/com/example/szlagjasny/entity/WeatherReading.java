package com.example.szlagjasny.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class WeatherReading {
    @Id
    @GeneratedValue
    private long id;

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public long getId() {

        return id;
    }

    @Column

    private LocalDateTime timestamp;
}
