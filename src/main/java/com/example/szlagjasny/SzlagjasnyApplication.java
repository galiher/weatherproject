package com.example.szlagjasny;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class SzlagjasnyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SzlagjasnyApplication.class, args);
	}
}
