package com.example.szlagjasny.model;

import java.util.List;

public class WeatherResponse {
    private int cod;
    private double calctime;

    public int getCod() {
        return cod;
    }

    public void setCod(int cod) {
        this.cod = cod;
    }

    public double getCalctime() {
        return calctime;
    }

    public void setCalctime(double calctime) {
        this.calctime = calctime;
    }

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public List<CityWeatherData> getList() {
        return list;
    }

    public void setList(List<CityWeatherData> list) {
        this.list = list;
    }

    private int cnt;
    private List<CityWeatherData> list;
}
