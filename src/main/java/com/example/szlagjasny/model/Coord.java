package com.example.szlagjasny.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Coord {

    @JsonProperty("Lat")
    private Double lat;
    @JsonProperty("Lon")
    private Double lon;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }
}
