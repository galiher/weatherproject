package com.example.szlagjasny.controller;

import com.example.szlagjasny.entity.WeatherReading;
import com.example.szlagjasny.model.WeatherResponse;
import com.example.szlagjasny.service.WeatherService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

//TODO: Endpoint do pobierania temperatury dla miasta (z najnowszego odczytu), scache'owac (queryparam useChache czy nie)

@RestController
public class KurwaController {

    @Autowired
    WeatherService weatherService;

    @Value("${appid.key}")
    String key;

    @Value("${longitude.west}")
    String west;
    @Value("${longitude.east}")
    String east;
    @Value("${latitude.south}")
    String south;
    @Value("${latitude.north}")
    String north;
    @Value("${zoom}")
    String zoom;
    @Value(("${url.box}"))
    String boxUrl;

    private String getUrl() {
        List<String> coords = Arrays.asList(west, south, east, north, zoom);
        StringBuilder sb = new StringBuilder(boxUrl);
        sb.append(String.join(",", coords));
        sb.append("&APPID={key}");
        return sb.toString();
    }

    @RequestMapping("/get-weather")
    public String getDefault() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.TEXT_HTML_VALUE);
        HttpEntity<String> request = new HttpEntity<>(headers);
        String queryUrl = getUrl();
        WeatherResponse weatherResponse = new WeatherResponse();
        try {
            ResponseEntity<String> response2 = restTemplate.exchange(queryUrl, HttpMethod.GET, request, String.class, key);
            String body = response2.getBody();
            ObjectMapper mapper = new ObjectMapper();
            weatherResponse = mapper.readValue(body, WeatherResponse.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        weatherService.saveWeatherData(weatherResponse);

        return "Temporary stub";
    }

    @RequestMapping("show-weather")
    public String showWeather() {
        String warmestCity = "There has been an error while reading the data";
        Optional<WeatherReading> timestamp = weatherService.getNewestReading();
        if(timestamp.isPresent()) {

            if (timestamp.get().getTimestamp().isBefore(LocalDateTime.now().minusMinutes(1))) {
                getDefault();
                warmestCity = showWeather();

            } else {

                warmestCity = weatherService.getWarmestCity(timestamp.get().getId());
            }
        }
        return warmestCity;
    }

    private void setWarmestCity(Optional<WeatherReading> timestamp, String warmestCity) {
        if (timestamp.get().getTimestamp().isBefore(LocalDateTime.now().minusMinutes(1))) {
            getDefault();
            warmestCity = showWeather();

        } else {

            warmestCity = weatherService.getWarmestCity(timestamp.get().getId());
        }
    }

    @RequestMapping("show-weather-for-date/{date}/")
    public String showWeatherForTimeStamp(@PathVariable("date") String dateTime) {
        LocalDateTime timeStamp = LocalDateTime.parse(dateTime);
        String warmestCity = weatherService.getWeatherReadingsByTimestamp(timeStamp);
        return warmestCity;
    }

    @RequestMapping("show-temperature-in-a-city/{name}/")
    public String showTemperatureInACity(@PathVariable("name") String cityName) {
        Optional<WeatherReading> timestamp = weatherService.getNewestReading();
        if (timestamp.get().getTimestamp().isBefore(LocalDateTime.now().minusMinutes(1))) {
            weatherService.cleanCacheTempForTheCity();
        }
        return weatherService.getTemperatureForTheCity(cityName, timestamp.get().getId());
    }



}
