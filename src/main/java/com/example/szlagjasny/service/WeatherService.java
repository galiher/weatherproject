package com.example.szlagjasny.service;


import com.example.szlagjasny.entity.CityTemperature;
import com.example.szlagjasny.entity.WeatherReading;
import com.example.szlagjasny.model.WeatherResponse;
import com.example.szlagjasny.repository.TimestampRepository;
import com.example.szlagjasny.repository.WeatherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class WeatherService {

    private WeatherRepository weatherRepository;
    private TimestampRepository timestampRepository;

    @Autowired
    public WeatherService(WeatherRepository weatherRepository, TimestampRepository repository) {
        this.weatherRepository = weatherRepository;
        this.timestampRepository = repository;

    }

    public void saveWeatherData(WeatherResponse response) {
        LocalDateTime now = LocalDateTime.now();
        now = now.withSecond(0).withNano(0);
        WeatherReading reading = new WeatherReading();
        reading.setTimestamp(now);
        timestampRepository.save(reading);
        response.getList()
                .stream()
                .map(p -> new CityTemperature().setName(p.getName()).setTemperature(p.getMain().getTemp()).setReadingId(reading.getId()))
                .forEach(weatherRepository::save);
    }

    public Optional<WeatherReading> getNewestReading() {
        List<WeatherReading> weatherReadings = new ArrayList<>();
        timestampRepository.findAll().forEach(weatherReadings::add);
        Optional<WeatherReading> timestamp = weatherReadings.stream()
                .sorted((p, q) -> {
                    return (q.getTimestamp().compareTo(p.getTimestamp()));
                })
                .findFirst();
        return timestamp;
    }

    public String getWarmestCity(long id) {
        String warmestCity = "";
        List<CityTemperature> ct = new ArrayList<>();
        List<CityTemperature> c = new ArrayList<>();
        weatherRepository.findAll().forEach(c::add);
        weatherRepository.getCityTemperaturesByReadingId(id).forEach(ct::add);
        Optional<CityTemperature> result = ct.stream().sorted((p, q) -> {
            return (q.getTemperature().compareTo(p.getTemperature()));
        }).findFirst();
        warmestCity = result.orElse(new CityTemperature().setName("Error")).getName() + " with temperature of: " +
                result.orElse(new CityTemperature().setTemperature(0.0F)).getTemperature() +
                " is the warmest city";
        return warmestCity;
    }

    public String getWeatherReadingsByTimestamp(LocalDateTime localDateTime) {
        List<WeatherReading> weatherReadings = timestampRepository.getWeatherReadingsById(localDateTime);

        return getWarmestCity(weatherReadings.stream().findFirst().orElse(null).getId());
    }

    @Cacheable("nametemperature")
    public String getTemperatureForTheCity(String cityName, Long readingId) {

        StringBuilder tempInCity = new StringBuilder("The temperature in ");
        CityTemperature cT = weatherRepository.getCityTemperaturesByName(cityName, readingId).get(0);
        tempInCity.append(cT.getName());
        tempInCity.append(" is: ");
        tempInCity.append(cT.getTemperature());
        return tempInCity.toString();
    }

    @CacheEvict("nametemperature")
    public void cleanCacheTempForTheCity() {

    }

}

