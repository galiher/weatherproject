package com.example.szlagjasny.repository;

import com.example.szlagjasny.entity.WeatherReading;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface TimestampRepository extends CrudRepository<WeatherReading, Long> {

    @Query("select t from WeatherReading t where t.timestamp = :timestamp")
    public List<WeatherReading> getWeatherReadingsById(@Param("timestamp") LocalDateTime localDateTime);

}
