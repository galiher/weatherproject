package com.example.szlagjasny.repository;

import com.example.szlagjasny.entity.CityTemperature;
import com.example.szlagjasny.entity.TestData;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WeatherRepository extends JpaRepository<CityTemperature, Long> {

    @Query("select t from CityTemperature t where t.readingId = :id")
    public List<CityTemperature> getCityTemperaturesByReadingId(@Param("id")Long id);

    @Query("select t from CityTemperature t where t.name = :name and t.readingId = :readingId")
    public List<CityTemperature> getCityTemperaturesByName(@Param("name") String name, @Param("readingId") Long readingId);

}
